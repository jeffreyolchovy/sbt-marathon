# Changelog

## 0.1.1
- Add `marathonSetServiceUrl` command
  - Allows users to change the `marathonServiceUrl` value during an sbt session
  - Useful for targeting multiple Mesos clusters / Marathon instances during application deployment

## 0.1.0
- First release
